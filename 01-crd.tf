resource "kubernetes_manifest" "crd-ingressroute" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "apiextensions.k8s.io/v1beta1"
    "kind" = "CustomResourceDefinition"
    "metadata" = {
      "name" = "ingressroutes.traefik.containo.us"
    }
    "spec" = {
      "group" = "traefik.containo.us"
      "names" = {
        "kind" = "IngressRoute"
        "plural" = "ingressroutes"
        "singular" = "ingressroute"
      }
      "scope" = "Namespaced"
      "version" = "v1alpha1"
    }
  }
}


resource "kubernetes_manifest" "crd-ingressroutetcp" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "apiextensions.k8s.io/v1beta1"
    "kind" = "CustomResourceDefinition"
    "metadata" = {
      "name" = "ingressroutetcps.traefik.containo.us"
    }
    "spec" = {
      "group" = "traefik.containo.us"
      "names" = {
        "kind" = "IngressRouteTCP"
        "plural" = "ingressroutetcps"
        "singular" = "ingressroutetcp"
      }
      "scope" = "Namespaced"
      "version" = "v1alpha1"
    }

  }
}

resource "kubernetes_manifest" "crd-middleware" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "apiextensions.k8s.io/v1beta1"
    "kind" = "CustomResourceDefinition"
    "metadata" = {
      "name" = "middlewares.traefik.containo.us"
    }
    "spec" = {
      "group" = "traefik.containo.us"
      "names" = {
        "kind" = "Middleware"
        "plural" = "middlewares"
        "singular" = "middleware"
      }
      "scope" = "Namespaced"
      "version" = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "crd-tlsoption" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "apiextensions.k8s.io/v1beta1"
    "kind" = "CustomResourceDefinition"
    "metadata" = {
      "name" = "tlsoptions.traefik.containo.us"
    }
    "spec" = {
      "group" = "traefik.containo.us"
      "names" = {
        "kind" = "TLSOption"
        "plural" = "tlsoptions"
        "singular" = "tlsoption"
      }
      "scope" = "Namespaced"
      "version" = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "crd-traefikservice" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "apiextensions.k8s.io/v1beta1"
    "kind" = "CustomResourceDefinition"
    "metadata" = {
      "name" = "traefikservices.traefik.containo.us"
    }
    "spec" = {
      "group" = "traefik.containo.us"
      "names" = {
        "kind" = "TraefikService"
        "plural" = "traefikservices"
        "singular" = "traefikservice"
      }
      "scope" = "Namespaced"
      "version" = "v1alpha1"
    }

  }
}
