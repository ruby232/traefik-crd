resource "kubernetes_deployment" "entrypoint" {
  metadata {
    name = "traefik-app"
    namespace = var.entrypoint_namespace
    labels = {
      app = "traefik"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "traefik"
      }
    }

    template {
      metadata {
        labels = {
          name = "entrypoint"
          app = "traefik"
        }
      }

      spec {
        service_account_name = kubernetes_service_account.entrypoint.metadata.0.name
        termination_grace_period_seconds = 60
        container {
          image = "traefik:2.2"
          name = "entrypoint"

          env {
            name = "TRAEFIK_API"
            value = "true"
          }

          env {
            name = "TRAEFIK_LOG"
            value = "true"
          }

          env {
            name = "TRAEFIK_LOG_LEVEL"
            value = "DEBUG"
          }

          env {
            name = "TRAEFIK_ENTRYPOINTS_web"
            value = "true"
          }

          env {
            name = "TRAEFIK_ENTRYPOINTS_web_ADDRESS"
            value = "8000"
          }

          env {
            name = "TRAEFIK_ENTRYPOINTS_web_HTTP_REDIRECTIONS_ENTRYPOINT_TO"
            value = "websecure"
          }

          env {
            name = "TRAEFIK_ENTRYPOINTS_websecure"
            value = "true"
          }

          env {
            name = "TRAEFIK_ENTRYPOINTS_websecure_ADDRESS"
            value = "4443"
          }

          env {
            name = "TRAEFIK_PROVIDERS_KUBERNETESCRD"
            value = "true"
          }

          env {
            name = "TRAEFIK_CERTIFICATESRESOLVERS_le"
            value = "true"
          }
          env {
            name = "TRAEFIK_CERTIFICATESRESOLVERS_le_ACME_EMAIL"
            value = "ruby@digitalprojex.com"
          }

          port {
            container_port = 8000
          }

          port {
            container_port = 4443
          }

          port {
            container_port = 8080
          }
        }
      }
    }
  }
}
