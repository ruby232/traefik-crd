resource "kubernetes_cluster_role" "entrypoint" {
  metadata {
    name = "traefik-ingress-controller"
  }

  rule {
    api_groups = [""]
    resources  = ["services", "endpoints", "secrets"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }

  rule {
    api_groups = ["traefik.containo.us"]
    resources  = ["middlewares"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["traefik.containo.us"]
    resources  = ["ingressroutes"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["traefik.containo.us"]
    resources  = ["ingressroutetcps"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["traefik.containo.us"]
    resources  = ["tlsoptions"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["traefik.containo.us"]
    resources  = ["traefikservices"]
    verbs      = ["get", "list", "watch"]
  }
}


resource "kubernetes_cluster_role_binding" "entrypoint" {
  metadata {
    name = "traefik-ingress-controller"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "traefik-ingress-controller"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "traefik-ingress-controller"
    namespace = var.entrypoint_namespace
  }
}
