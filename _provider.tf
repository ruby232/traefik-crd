provider "kubernetes" {
  config_path = "kubeconfig.yml"
}

provider "kubernetes-alpha" {
  config_path = "kubeconfig.yml"
}
