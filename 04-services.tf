resource "kubernetes_service_account" "entrypoint" {
  metadata {
    name = "traefik-ingress-controller"
    namespace = var.entrypoint_namespace
  }
}

resource "kubernetes_service" "entrypoint" {
  metadata {
    name = "entrypoint-service"
    namespace = var.entrypoint_namespace
  }
  spec {
    selector = {
      app = kubernetes_deployment.entrypoint.metadata.0.labels.app
    }
    session_affinity = "ClientIP"
    port {
      protocol = "TCP"
      port = 8000
      name = "web"
      target_port= 8000
      node_port =  30080
    }

    port {
      protocol = "TCP"
      port = 8080
      name = "admin"
      target_port = 8080
    }

    port {
      protocol = "TCP"
      port = 4443
      name = "websecure"
      target_port = 4443
      node_port = 30443
    }

    type = "NodePort"
  }
}
