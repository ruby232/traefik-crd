resource "kubernetes_service" "tcp-loadbalancer" {
  metadata {
    name = "tcp-loadbalancer"
    namespace = var.entrypoint_namespace
  }
  spec {
    selector = {
      app = kubernetes_deployment.entrypoint.metadata.0.labels.app
    }
    port {
      name = "http"
      protocol = "TCP"
      port = 80
      target_port= 8000
    }

    port {
      name = "https"
      protocol = "TCP"
      port = 443
      target_port = 4443
    }
    type = "LoadBalancer"
  }
}
