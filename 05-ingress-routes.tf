resource "kubernetes_manifest" "ingressroute-web" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind" = "IngressRoute"
    "metadata" = {
      "name" = "traefik-admin"
      "namespace" = var.entrypoint_namespace
    }
    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          kind = "Rule"
          match = "Host(`my-traefik.com`)"
          services = [
            {
              name = "entrypoint-service"
              port = 8080
            },
          ]
        },
      ]
    }
  }
}

resource "kubernetes_manifest" "ingressroute-websecure" {
  provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind" = "IngressRoute"
    "metadata" = {
      "name" = "traefik-admin-tls"
      "namespace" = var.entrypoint_namespace
    }
    "spec" = {
      "entryPoints" = [
        "websecure",
      ]
      "routes" = [
        {
          kind = "Rule"
          match = "Host(`traefik-kl.digitalprojex.com`)"
          services = [
            {
              name = "entrypoint-service"
              port = 8080
            },
          ]
        },
      ]
      "tls" = {
        "certResolver" = "le"
      }
    }
  }
}
